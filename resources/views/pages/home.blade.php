@extends('layouts/main')
@section('content')

    <div class="container ">

    @foreach($posts as $post)

        <div class="col-md-4">
            <h6>Kategorija: {{$post->cat}}</h6>
            <h2>{{$post->name}}</h2>
            <p>{{str_limit($post->desc,100)}} </p>
            <p><a class="btn btn-default" href="post/{{$post->id}}" role="button">Rodyti visa informacija &raquo;</a></p>
        </div>
    @endforeach


    </div>
    <div class="container ">
        <div class="links">
            {{$posts->links()}}

        </div>
    </div>


@endsection
