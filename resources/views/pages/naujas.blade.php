@extends('layouts/main')
@section('content')

    @include('includes/errors')
    <div class="content">
        <div class="title m-b-md">

            <h2>Naujas skelbimas</h2>
        </div>
        <form action="/irasai" method="post" class="form-horizontal">
            {{csrf_field()}}

            <label for="name">Kategorija</label>
            <select id="cat" name="cat" >

                @foreach($cats as $cat)

                <option value="{{$cat->cat}}">{{$cat->cat}}</option>

                @endforeach

            </select>



            <label for="name">Pavadinimas</label>
            <input type="text" name="name" id="name">
            <label for="text">Aprasymas</label>
            <input type="text" name="desc" id="desc">
            <button type="submit" name="submit" value="submit">Submit</button>
        </form>
    </div>

@endsection