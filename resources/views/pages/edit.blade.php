@extends('layouts/main')
@section('content')

    @include('includes/errors')
    <div class="content">
        <div class="title m-b-md">

            <h2>Nauji įrašai</h2>
        </div>
        <form action="/post/{{$post->id}}" method="post" class="form-horizontal">
            {{csrf_field()}}
            {{method_field('PATCH')}}
            <label for="name">pavadinimas</label>
            <input type="text" name="name" id="name" value="{{$post->name}}">
            <label for="text">Jūsų tekstas</label>
            <input type="text" name="desc" id="desc" value="{{$post->desc}}">
            <button type="submit" name="submit" value="submit">Submit</button>
        </form>
    </div>

@endsection