
@extends('layouts/main')
@section('content')
    <h6>Kategorija: {{$post->cat}}</h6>
    <h2>{{$post->name}}</h2>
    <p>{{$post->desc}}</p>




    @if(Auth::id()==$post->user_id)
        <p><a class="btn btn-default" href="/post/{{$post->id}}/edit" role="button">Edit</a></p>
        <p><a class="btn btn-default" href="/post/{{$post->id}}/delete" role="button">Delete</a></p>
    @endif

@endsection
